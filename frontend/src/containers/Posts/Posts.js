import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {deletePost, fetchPosts} from "../../store/actions/postsActions";
import {Button, Grid, Typography} from "@material-ui/core";
import PostItem from "../../components/PostItem/PostItem";

const Posts = () => {
    const dispatch = useDispatch();
    const posts = useSelector(state => state.posts.posts);

    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch]);

    const removePost = (id) => {
        dispatch(deletePost(id));
    };

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justifyContent="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">Posts</Typography>
                </Grid>
                <Grid item>
                    <Button color="primary" component={Link} to="/posts/new">Add</Button>
                </Grid>
            </Grid>
            <Grid item container direction="row" spacing={1}>
                {posts.map(post => (
                    <PostItem
                        key={post.id}
                        id={post.id}
                        title={post.title}
                        dateTime={post.date_time}
                        image={post.image}
                        remove={removePost}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default Posts;