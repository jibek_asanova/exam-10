import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPost} from "../../store/actions/postsActions";
import {Box, Paper, Typography} from "@material-ui/core";
import {createComment, deleteComment, fetchComments} from "../../store/actions/commentsActions";
import Grid from "@material-ui/core/Grid";
import CommentItem from "../../components/CommentItem/CommentItem";
import CommentForm from "../../components/CommentForm/CommentForm";

const Post = ({match}) => {
    const dispatch = useDispatch();
    const post = useSelector(state => state.posts.post);
    const comments = useSelector(state => state.comments.comments);

    useEffect(() => {
        dispatch(fetchPost(match.params.id))
    }, [dispatch, match.params.id]);

    useEffect(() => {
        dispatch(fetchComments(match.params.id));
    }, [dispatch, match.params.id]);

    const onSubmit = async commentData => {
        commentData.post_id = match.params.id;

        await dispatch(createComment(commentData, match.params.id));
    };

    const removeComment = (id,post_id) => {
        dispatch(deleteComment(id, post_id));
    };

    return post && (
        <Grid container direction="column" spacing={2}>
            <Paper component={Box} p={2}>
                <Typography variant="h4">{post.title}</Typography>
                <Typography variant="subtitle1">At {post.date_time}</Typography>
                <Typography variant="body1">{post.description}</Typography>
            </Paper>
            <Grid item>
                <Typography variant="h4">Comments</Typography>
            </Grid>
            <Grid item container direction="column" spacing={1}>
                {comments.map(comment => (
                    <CommentItem
                        key={comment.id}
                        id={comment.id}
                        post_id={match.params.id}
                        author={comment.author}
                        comment={comment.comment}
                        remove={removeComment}
                    />
                ))}
            </Grid>
            <Grid item>
                <Typography variant="h4">Create Comment</Typography>
            </Grid>
            <CommentForm
                onSubmit={onSubmit}
            />
        </Grid>
    );
};

export default Post;