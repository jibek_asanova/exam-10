import React from "react";
import Typography from "@material-ui/core/Typography";
import {useDispatch} from "react-redux";
import PostForm from "../../components/PostForm/PostForm";
import {createPost} from "../../store/actions/postsActions";

const NewPost = ({history}) => {
    const dispatch = useDispatch();

    const onSubmit = async postData => {
        await dispatch(createPost(postData));
        history.replace('/');
    };

    return (
        <>
            <Typography variant="h4">New post</Typography>
            <PostForm
                onSubmit={onSubmit}
            />
        </>
    );
};

export default NewPost;