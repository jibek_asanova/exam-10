import {
    DELETE_COMMENT_FAILURE,
    DELETE_COMMENT_REQUEST, DELETE_COMMENT_SUCCESS,
    FETCH_COMMENTS_FAILURE,
    FETCH_COMMENTS_REQUEST, FETCH_COMMENTS_SUCCESS,
} from "../actions/commentsActions";


const initialState = {
    fetchLoading: false,
    singleLoading: false,
    comments: [],
};

const commentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, fetchLoading: false, comments: action.payload};
        case FETCH_COMMENTS_FAILURE:
            return {...state, fetchLoading: false};
        case DELETE_COMMENT_REQUEST:
            return {...state, loading: true};
        case DELETE_COMMENT_SUCCESS:
            return {...state, loading: false};
        case DELETE_COMMENT_FAILURE:
            return {...state, loading: false}
        default:
            return state;
    }
};

export default commentsReducer;