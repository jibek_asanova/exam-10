import axios from "axios";


export const FETCH_COMMENTS_REQUEST = 'FETCH_COMMENTS_REQUEST';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENTS_FAILURE = 'FETCH_COMMENTS_FAILURE';

export const CREATE_COMMENT_REQUEST = 'CREATE_COMMENT_REQUEST';
export const CREATE_COMMENT_SUCCESS = 'CREATE_COMMENT_SUCCESS';
export const CREATE_COMMENT_FAILURE = 'CREATE_COMMENT_FAILURE';

export const DELETE_COMMENT_REQUEST = 'DELETE_COMMENT_REQUEST';
export const DELETE_COMMENT_SUCCESS = 'DELETE_COMMENT_SUCCESS';
export const DELETE_COMMENT_FAILURE = 'DELETE_COMMENT_FAILURE';

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, payload: comments});
export const fetchCommentsFailure = () => ({type: FETCH_COMMENTS_FAILURE});

export const createCommentRequest = () => ({type: CREATE_COMMENT_REQUEST});
export const createCommentSuccess = () => ({type: CREATE_COMMENT_SUCCESS});
export const createCommentFailure = () => ({type: CREATE_COMMENT_FAILURE});

export const deleteCommentRequest = () => ({type: DELETE_COMMENT_REQUEST});
export const deleteCommentSuccess = comment => ({type: DELETE_COMMENT_SUCCESS, payload: comment});
export const deleteCommentFailure = () => ({type: DELETE_COMMENT_FAILURE});

export const fetchComments = (id) => {
    return async dispatch => {
        try {
            dispatch(fetchCommentsRequest());
            const response = await axios.get('http://localhost:8000/comments?post_id=' + id);
            dispatch(fetchCommentsSuccess(response.data));
        } catch (e) {
            dispatch(fetchCommentsFailure());
        }
    };
};

export const createComment = (commentData, post_id) => {
    return async dispatch => {
        try {
            dispatch(createCommentRequest());
            await axios.post('http://localhost:8000/comments', commentData);
            dispatch(fetchComments(post_id));
            dispatch(createCommentSuccess());
        } catch (e) {
            dispatch(createCommentFailure());
            throw e;
        }
    };
};

export const deleteComment = (id,post_id) => {
    return async (dispatch) => {
        dispatch(deleteCommentRequest());
        try {
            const response = await axios.delete('http://localhost:8000/comments/' + id);
            dispatch(deleteCommentSuccess(response.data));
            dispatch(fetchComments(post_id));
        } catch(error) {
            dispatch(deleteCommentFailure(error));
        }
    }
};