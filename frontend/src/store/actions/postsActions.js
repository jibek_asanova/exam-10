import axios from "axios";

export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';

export const FETCH_POST_REQUEST = 'FETCH_POST_REQUEST';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const FETCH_POST_FAILURE = 'FETCH_POST_FAILURE';

export const CREATE_POST_REQUEST = 'CREATE_POST_REQUEST';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const CREATE_POST_FAILURE = 'CREATE_POST_FAILURE';

export const DELETE_POST_REQUEST = 'DELETE_POST_REQUEST';
export const DELETE_POST_SUCCESS = 'DELETE_POST_SUCCESS';
export const DELETE_POST_FAILURE = 'DELETE_POST_FAILURE';

export const fetchPostsRequest = () => ({type: FETCH_POSTS_REQUEST});
export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, payload: posts});
export const fetchPostsFailure = () => ({type: FETCH_POSTS_FAILURE});

export const fetchPostRequest = () => ({type: FETCH_POST_REQUEST});
export const fetchPostSuccess = post => ({type: FETCH_POST_SUCCESS, payload: post});
export const fetchPostFailure = () => ({type: FETCH_POST_FAILURE});

export const createPostRequest = () => ({type: CREATE_POST_REQUEST});
export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});
export const createPostFailure = () => ({type: CREATE_POST_FAILURE});

export const deletePostRequest = () => ({type: DELETE_POST_REQUEST});
export const deletePostSuccess = post => ({type: DELETE_POST_SUCCESS, payload: post});
export const deletePostFailure = () => ({type: DELETE_POST_FAILURE});

export const fetchPosts = () => {
    return async dispatch => {
        try {
            dispatch(fetchPostsRequest());
            const response = await axios.get('http://localhost:8000/posts');
            dispatch(fetchPostsSuccess(response.data));
        } catch (e) {
            dispatch(fetchPostsFailure());
        }
    };
};

export const fetchPost = id => {
    return async dispatch => {
        try {
            dispatch(fetchPostRequest());
            const response = await axios.get('http://localhost:8000/posts/' + id);
            dispatch(fetchPostSuccess(response.data));
        } catch (e) {
            dispatch(fetchPostFailure());
        }
    };
};

export const createPost = postData => {
    return async dispatch => {
        try {
            dispatch(createPostRequest());
            await axios.post('http://localhost:8000/posts', postData);
            dispatch(createPostSuccess());
        } catch (e) {
            dispatch(createPostFailure());
            throw e;
        }
    };
};

export const deletePost = (id) => {
    return async (dispatch) => {
        dispatch(deletePostRequest());
        try {
            const response = await axios.delete('http://localhost:8000/posts/' + id);
            dispatch(deletePostSuccess(response.data));
            dispatch(fetchPosts());
        } catch(error) {
            dispatch(deletePostFailure(error));
        }
    }
};