import {Link} from "react-router-dom";
import {
    Button,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Grid,
    IconButton,
    makeStyles,
    Typography
} from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import imageNotAvailable from '../../assets/no-image-available-icon-photo-camera-flat-vector-illustration-132483141.jpg';
import {apiURL} from "../../config";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%' // 16:9
    }
});

const PostItem = ({title, dateTime, id, image, remove}) => {
    const classes = useStyles();

    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + '/uploads/' + image;
    }

    return (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={title}/>
                <CardMedia
                    image={cardImage}
                    title={title}
                    className={classes.media}
                />
                <CardContent>
                    <Typography variant="subtitle1">
                        Time: {dateTime}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Typography variant="subtitle1">
                        Read full post
                    </Typography>
                    <IconButton component={Link} to={'/posts/' + id}>
                        <ArrowForwardIcon />
                    </IconButton>
                </CardActions>
                <Button color="primary" onClick={() => remove(id)} >Delete</Button>
            </Card>
        </Grid>
    );
};

export default PostItem;