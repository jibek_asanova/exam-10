import {
    Box, Button, Paper,
    Typography
} from "@material-ui/core";

import React from "react";


const CommentItem = ({author, comment, id, remove, post_id}) => {

    return (
        <Paper component={Box} p={2}>
            <Typography variant="h4">Author: {author}</Typography>
            <Typography variant="body1">Comment: {comment}</Typography>
            <Button color="primary" onClick={() => remove(id, post_id)} >Delete</Button>
        </Paper>
    );
};

export default CommentItem;