import {Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Posts from "./containers/Posts/Posts";
import Post from "./containers/Post/Post";
import NewPost from "./containers/NewPost/NewPost";


const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Posts}/>
            <Route path="/posts/new" component={NewPost}/>
            <Route path="/posts/:id" component={Post}/>
        </Switch>
    </Layout>
);

export default App;