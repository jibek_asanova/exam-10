import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import postsReducer from "./store/reducers/postsReducers";
import {MuiThemeProvider} from "@material-ui/core";
import theme from "./theme";
import commentsReducer from "./store/reducers/commentsReducers";

const rootReducer = combineReducers({
    posts: postsReducer,
    comments: commentsReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));

const app = (
    <Provider store={store}>
        <MuiThemeProvider theme={theme}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
        </MuiThemeProvider>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));

