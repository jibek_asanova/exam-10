const express = require('express');
const cors = require('cors');
const posts = require('./app/posts');
const comments = require('./app/comments');
const mysqlDb = require('./mySqlDb');

const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const port = 8000;

app.use('/posts', posts);
app.use('/comments', comments);

mysqlDb.connect().catch(e => console.log(e));
app.listen(port, () => {
  console.log(`Server started on ${port} port!`);
});