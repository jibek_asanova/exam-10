DROP DATABASE IF EXISTS news;

CREATE DATABASE IF NOT EXISTS news;

use news;

CREATE TABLE posts (
    id int auto_increment,
    title varchar(255) not null,
    description text not null,
    image varchar(255) null,
    date_time datetime default current_timestamp not null,
    constraint  posts_pk primary key (id)
);

CREATE TABLE comments (
    id int auto_increment,
    post_id int not null,
    author varchar(255) default 'anonymous',
    comment text not null,
    constraint  posts_pk primary key (id),
    constraint posts_comments_id_fk
    foreign key (post_id)
    references  posts (id)
    on update cascade
    on delete restrict
);

INSERT INTO posts (title, description, image)
VALUES ('post 1', 'test', null),
       ('post 3', 'test test', null),
       ('post 4', 'test test test', null);

INSERT INTO comments (post_id,comment)
VALUES (1, 'hey');

INSERT INTO comments (post_id,comment)
VALUES (1, 'hello world'),
       (1, 'hello world 2');








