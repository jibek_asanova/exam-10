const mysqlDb = require('mysql2/promise');
const config = require('./config');

let connection = null;

module.exports = {
  connect: async () => {
    connection = await mysqlDb.createConnection(config.databaseOptions);
    console.log('success', connection.threadId);
  },
  getConnection: () => connection
};