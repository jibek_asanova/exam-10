const express = require('express');
const mysqlDb = require('../mySqlDb');

const router = express.Router();

router.get('/', async (req, res) => {
  const [comments] = await mysqlDb.getConnection().query('SELECT * from comments');

  if(req.query.post_id) {
    const [comments] = await mysqlDb.getConnection().query(
      'SELECT * from ?? where post_id = ?',
      ['comments', req.query.post_id]
    );
    return res.send(comments);
  }

  res.send(comments);
});

router.post('/', async (req, res) => {
  if (!req.body.author || !req.body.comment || !req.body.post_id) {
    return res.status(400).send({error: 'Data not valid'});
  }

  const comment = {
    author: req.body.author,
    comment: req.body.comment,
    post_id: req.body.post_id,
  };

  const newComment = await mysqlDb.getConnection().query(
    'INSERT INTO ?? (post_id, author, comment) values (?, ?, ?)',
    ['comments', comment.post_id, comment.author, comment.comment]
  );

  res.send({
    ...comment,
    id: newComment[0].insertId
  });
});

router.delete('/:id', async (req, res) => {
  try{
    await mysqlDb.getConnection().query(
      'DELETE  FROM ??  where id = ?',
      ['comments', req.params.id]);
    res.send({message: `Delete successful, id= ${req.params.id}`});
  } catch (e) {
    return res.status(400).send({error: 'Error: Cannot delete or update a parent row'});
  }
});

module.exports = router;


