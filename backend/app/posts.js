const express = require('express');
const mysqlDb = require('../mySqlDb');
const path = require("path");
const multer = require("multer");
const config = require('../config');
const {nanoid} = require("nanoid");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  const [posts] = await mysqlDb.getConnection().query('SELECT * from posts');
  res.send(posts);
});


router.get('/:id', async (req, res) => {
  const [post] = await mysqlDb.getConnection().query(
    'SELECT * FROM ?? where id = ?',
    ['posts', req.params.id])
  if (!post) {
    return res.status(404).send({error: 'post not found'});
  }

  res.send(post[0]);
});

router.post('/', upload.single('image'), async (req, res) => {
  if (!req.body.title  || !req.body.description) {
    return res.status(400).send({error: 'Data not valid'});
  }

  const post = {
    title: req.body.title,
    description: req.body.description,
  };

  if (req.file) {
    post.image = req.file.filename;
  }

  const newPost = await mysqlDb.getConnection().query(
    'INSERT INTO ?? (title, description, image) values (?, ?, ?)',
    ['posts', post.title, post.description, post.image]
  );

  res.send({
    ...post,
    id: newPost[0].insertId
  });
});


router.delete('/:id', async (req, res) => {
  try{
    await mysqlDb.getConnection().query(
      'DELETE  FROM ??  where id = ?',
      ['posts', req.params.id]);
    res.send({message: `Delete successful, id= ${req.params.id}`});
  } catch (e) {
    return res.status(400).send({error: 'Error: Cannot delete or update a parent row'});
  }
});

module.exports = router;
